# Test task - Skyeng

* Namespace - хотя в PRS и нет указания писать верхний уровень с большой буквы, однако есть такое требование к названиям классов, а также все уровни Namespace в примерах пишутся с большой буквы. Так не src\Integration a, например App\Integration
* Тайп хинтинг - необходимо добавить в конструктор и в методах класса. Кроме того нужно указать тип возвращаемых методами значений.
* В аннотациях нужно добавить описание переменных класса и методов
* В классе  DataProvider в конструктор лучше передавать сконфигурированный http клиент и использовать его для работы с REST API
* Если принято решение использовать паттерн декоратор - необходимо:
  * создать общий интерфейс DataProviderInterface
  * указать implement DataProviderInterface у класса DataProvider
и DecoratorManager
  * В конструктор DecoratorManager нужно передать DataProvider 
  * DecoratorManager должен реализовывать тот же метод что и DataProvider 
* Для класса DecoratorManager  можно использовать метод setLogger но при этом желательно указать implement LoggerAwareInterface. Также можно указать logger в конструкторе.
* Для класса DecoratorManager  при работе с кэшэм лучше  использовать expiresAfter  вместо expiresAt. Период можно хранить в константе класса. После создания cacheItem нужно добавить ее в кэш ( $cache->save($item))
