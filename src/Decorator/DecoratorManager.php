<?php

declare(strict_types=1);

namespace App\Decorator;

use App\Integration\DataProvider;
use App\Integration\DataProviderInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Throwable;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Cache\InvalidArgumentException;

class DecoratorManager implements DataProviderInterface
{
    private const CACHE_TTL = 'P1D';

    /** @var CacheItemPoolInterface * */
    private $cache;

    /**@var  LoggerInterface * */
    private $logger;

    /** @var DataProvider */
    private $provider;


    /**
     * DecoratorManager constructor.
     * @param CacheItemPoolInterface $cache
     * @param LoggerInterface $logger
     * @param DataProvider $provider
     */
    public function __construct(CacheItemPoolInterface $cache, LoggerInterface $logger, DataProvider $provider)
    {
        $this->cache = $cache;
        $this->logger = $logger;
        $this->provider = $provider;

    }

    /**
     * @param array $array
     *
     * @return string
     */
    public function getCacheKey(array $array): string
    {
        $string = \GuzzleHttp\json_encode($array);
        return md5($string);
    }

    /**
     * @param array $data
     *
     * @return array
     *
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function get(array $data): array
    {
        try {
            $cacheKey = $this->getCacheKey($data);
            $cacheItem = $this->cache->getItem($cacheKey);
            if (!$cacheItem->isHit()) {
                $result = $this->provider->get($data);
                $interval = new \DateInterval(self::CACHE_TTL);
                $cacheItem->set($result)->expiresAfter($interval);
                $this->cache->save($cacheItem);
            } else {
                $result = $cacheItem->get();
            }
            return $result;
        } catch (Throwable $e) {
            $this->logger->critical($e->getMessage(), $e->getTraceAsString());
        }
    }
}
