<?php

declare(strict_types=1);

namespace App\Integration;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class DataProvider implements DataProviderInterface
{
    const REST_URL = '/api/some/url';
    const REST_METHOD = 'GET';

    /** @var  ClientInterface */
    private $client;

    /**
     * DataProvider constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $data
     *
     * @return array
     *
     * @throws GuzzleException
     */
    public function get(array $data): array
    {
        $url = http_build_url(self::REST_URL, $data);
        $response = $this->client->request(self::REST_METHOD, $url);
        $content = $response->getBody()->getContents();
        return json_decode($content, true);
    }
}
