<?php
namespace App\Integration;

interface DataProviderInterface
{

    /**
     * @param array $data
     *
     * @return array
     */
    public function get(array $data): array;
}
